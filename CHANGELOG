### 3.83 (2024-05-29)

• Turkish Bible: New Turkish Bible 2001

### 3.82 (2024-04-22)

• Support for Android 14.0 (U): Target
• Android TV: "Now Playing" card

### 3.81 (2024-04-07)

• Fix: Chromebook

### 3.80 (2024-03-27)

• Button: Content
• Audio Bible is modal
• Fix: Black theme, Android TV

### 3.79 (2024-01-31)

• Menu: Listen

### 3.78 (2024-01-24)

• Fix: Audio Bible

Rem: To have the best user experience, you have to set the app as a "App not optimised" in App Info/Battery/Optimise battery usage.

### 3.77 (2024-01-15)

• Interlinear: Open all references of a word

### 3.76 (2023-12-27)

• Menu: Lexicons

### 3.75 (2023-12-20)

• Rebuild of 3.74 to solve F-Droid problem

### 3.74 (2023-12-06)

Interlinear (Strongs concordance in English):
• Click on H9999-G9999/Hebrew/Greek words for detail
• Next/Previous

### 3.73 (2023-11-21)

• Contextual menu: Interlinear (Strongs concordance in English)
• Click on H9999-G9999 words for detail
• Hebrew/Greek/English Lexicons
(Big Thanks to STEPBible.org for their work)

### 3.72 (2023-11-01)

• Hebrew Bible: Westminster Leningrad Codex
• Greek Bible: Textus Receptus Stephanus 1550

### 3.71 (2023-08-20)

• More favorites

### 3.70 (2023-08-10)

• Tools: Import/export database

### 3.69 (2023-07-21)

• Support for Android 13.0 (T): Target
• Layout change when Bibles are displayed on 1 column

### 3.68 (2023-05-26)

• Russian Bible: Russian Synodal Translation 1876

### 3.67 (2023-05-20)

• German Bible: Schlachter 1951
• Japanese Bible: New Japanese Bible 1973

### 3.66 (2023-05-13)

• Chinese Bible: Chinese Union Version Simplified

### 3.65 (2023-05-09)

• Arabic Bible: Smith & Van Dyke 1865
• New setting for Arabic/Hindi: Language of User interface

### 3.63 (2023-04-29)

• Hindi version
• Hindi Bible: Hindi Indian Revised Version 2017
(Big Thanks to SundeepDan for his help)

### 3.62 (2023-03-16)

• Italian Bible: Nuova Diodati 1991

### 3.61 (2023-03-09)

• Spanish Bible: Reina Valera Actualizada 1989

### 3.60 (2023-01-14)

• Share an image: Options

### 3.59 (2022-12-30)

• Share an image

### 3.58 (2022-11-28)

• Harmony of Gospels
• Contextual menu: Long click has been replaced by click
• About: Twitter

### 3.57 (2022-11-07)

• Random verses: Options

### 3.56 (2022-10-31)

• Random verses (reimplemented in app)

### 3.55 (2022-10-20)

• Support for Android 12.0 (S): Target

### 3.54 (2022-08-30)

• About: Website
• Article: How to be saved?
• Article: Additional tools

### 3.53 (2022-05-26)

• (English) Free ebook: The Rapture Survival Guide

### 3.49+ (2021-12-12)

• Fix: Android TV

### 3.48 (2021-12-06)

• Several fix
• Faster installation process

### 3.47 (2021-11-05)

• Support for Android 12.0 (S)

### 3.46 (2021-08-25)

• English Bible: KJV 2000

### 3.43 (2021-08-14)

• Tools: Close tab options
• Tools: Sort tabs
The tabs are grouped by plans, articles, parables and the rest.
Each group is sorted by book number, chapter number, verse number.

### 3.42 (2021-07-26)

• Search several expressions (see Help):
E.G: Matthew 12 Jesus,son of man
E.G: Jonah Jonah,fish, water,belly
E.G: Esau%jacob,abraham%sarah,isaac
• Search several expressions in Fav filter
• Settings: Search style

### 3.41 (2021-05-18)

• Several fix: Android TV
• Contact: Telegram
• Support for Android 12.0 (S): Test
• Support latest librairies

### 3.40 (2021-05-03)

• Plans: Checkbox in title
• Parables: Book name
• Articles/Parables: Browse with left/right
• Fix: Search

### 3.39 (2021-04-07)

• Fix: Open URL

### 3.38 (2021-03-23)

• Mode for one Bible or several (Settings/Mode)
• Content panel for fast scrolling (Swipe left)
• Youtubers

### 3.37 (2021-02-20)

• To-do list

### 3.36 (2021-01-26)

• Plans of reading: Create plans by chapter
• Several fix

### 3.35 (2021-01-19)

• Display support for visually impaired: fonts added, large fonts supported, colors updated, widgets updated

### 3.34 (2020-12-07)

• Articles & Youtubers

### 3.31 (2020-10-04)

• Support for Android 11.0 (R)

### 3.30 (2020-08-26)

• Close tabs (Tools menu): Easy to close dozens of tabs
• Clipboard (Contextual menu): Allows you to add several verses, chapters, cross references...
• Invite a friend: XDA added, Amazon removed (Amazon does not give support)
• Fix: Share...
• +15 Youtubers

### 3.29 (2020-06-28)

• Articles & space
• +4 Youtubers, +6 Podcasts

### 3.28 (2020-04-02)

• Articles

### 3.27 (2020-02-06)

• Fix: Context menu and D-pad left key
• Fix: Position in tab

### 3.26 (2019-12-29)

• Article: Podcasts
• Menu: Invite a friend

### 3.25 (2019-11-02)

• French Bible: Ostervald
• Fix: rotation

### 3.24 (2019-10-20)

• Widget for Audio Bible.
To set the widget: start listening in the application (Context menu/Listen)

### 3.23 (2019-10-13)

• Fix: Fastlane (F-Droid)

### 3.22 (2019-10-09)

• The widget is back: a random verse every hour :D

### 3.21 (2019-09-22)

• You do not have to restart the application when you change some preferences
• The language selected in the application may be different from the language of your system
• By selecting a Bible, this will define the language used in the application

### 3.20 (2019-09-18)

• Black theme
• Contact menu: Facebook

### 3.19 (2019-09-08)

• User Interface changes
• Preferences: New User Interface for Smartphone, Tablet (fixed menu)
• Preferences: Television Configuration (borders)
• Preferences: Font sizes (5..11)
• Contact menu: Github
• +1 Youtuber (this channel)

### 3.18 (2019-08-25)

• Support for Android 10.0 (Q)
• Notification to stop the Audio Bible

### 3.16 (2019-08-15)

• Cross-references (see the section in the help)

### 3.15 (2019-07-24)

• Fix: orientation change
• +4 Youtubers

### 3.14 (2019-07-17)

• User Interface changes (Television, Favorites)
• Fix: Settings

### 3.13 (2019-07-07)

• Support for Android 9.0 (Pie)
• Support for Chromebook
• Support for Android "Multi-Window" (Android 7.0+)
• +8 Youtubers

### 3.12 (2019-06-23)

• Audio Bible: read a whole book

### 3.11 (2019-06-16)

• Audio Bible (see the section in the help)

### 3.10 (2019-05-11)

• Creation of articles (see the section in the help)

### 3.9 (2018-01-27)

• +20 articles

### 3.8 (2018-12-16)

• +1 article
• +9 Youtubers

### 3.7 (2018-11-25)

• Fix: sound

### 3.6 (2018-11-18)

• Support for Android 8.0 (Oreo)
• Widget needs adaptation as Google Policies force all Android developers to target Android 8.0. I disabled the widget for the moment, need to learn. The widget should not block the release.

### 3.5 (2018-11-10)

• Portuguese version
• Portuguese Bible: Almeida Corrigida Fiel

### 3.4 (2018-10-14)

• 13 articles
• 7 Youtubers

### 3.3 (2018-06-10)

• Fix: menu
• Fix: Youtubers' article
• Fix (TV): search Favorites
• +1 Youtuber

### 3.2 (2018-05-05)

• Installation status

### 3.1 (2018-05-01)

• Compatible with Android set-top boxes via 2 user interfaces (Settings menu):
1) Smartphone, tablet
2) Television: for Android TV, Android set-top boxes, tablets, smarphones in landscape. Designed for television, there is a large border around the text for overscan.
• Gitlab button added (Contact menu)

### 3.0 (2018-04-22)

* Compatible with Android TV and D-pad  
(Long click on the tab title to close the tab)

3.14 - 20190717


EN:
• User Interface changes (Television, Favorites)
• Fix: Settings



FR:
• Changements dans l'interface utilisateur (Télévision, Favoris)
• Fix: Paramètres



IT:
• Cambiamenti nell'interfaccia utente (Televisione, Favoriti)
• Fix: Parametri



ES:
• Cambios en la interfaz de usuario (Televisión, Favoritos)
• Fix: Parámetros



PT:
• Mudanças na interface do usuário (Televisão, Favoritos)
• Fix: Configurações


Bible multi langues, gratuite, hors connection, sans publicité, entièrement en anglais, français, italien, espagnol, portugais, hindi.

King James Version, Segond, Diodati, Valera, Almeida, Ostervald, Schlachter, Bible arabe, Bible hindi, Bible chinoise, Bible japonaise, Bible russe, Bible turque.

Facile à utiliser avec des fonctions de recherches rapides, de partage, plans de lecture, Bible audio, articles, références croisées, harmonie des évangiles, versets aléatoires, interlinéaire (Concordance Strong en anglais).

Fonctionne aussi sur Android TV, Chromebook.

The Light est un puissant outil d’étude pour apprendre la Parole de Dieu.


